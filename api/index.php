<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
require 'Slim/Slim.php';
require 'dao/dao.php';
spl_autoload_register(function ($class) {
            include 'dao/' . $class . '.php';
        });

function jsonp_encode($object) {
    $json = json_encode($object);
    return isset($_GET['callback']) ? "{$_GET['callback']}($json)" : $json;
}

function outputJson($object) {
    header("Content-Type: application/json");
    echo jsonp_encode($object);

    exit;
}

$GLOBALS['WEIGHTINGS'] = array(0.4, 0.3, 0.15, 0.1, 0.05);

$app = new Slim();

$app->get('/crimes', function() {
            outputJson(Crime::getAllNeighbourhoodsNormalized());
        });


$app->get('/employment', function() {
            outputJson(Employment::getAllNeighbourhoodsNormalized());
        });

$app->get('/houseprices/b/:budget', function($budget) {
            outputJson(HousePrices::getAllNeighbourhoodsNormalizedByBudget($budget));
        });

$app->get('/employment/:neighbourhood', function ($neighbourhood) {
            $employmentOutput = new stdClass();
            $employmentOutput->type = 'unemployment_percent';
            $employmentOutput->number = Employment::getNeighbourhoodNormalized($neighbourhood);
            outputJson($employmentOutput);
        });

$app->get('/crimes/:neighbourhood', function ($neighbourhood) {
            $crimeOutput = new stdClass();
            $crimeOutput->type = 'crimes';
            $crimeOutput->number = Crime::getNeighbourhoodData($neighbourhood);

            outputJson($crimeOutput);
        });

$app->get('/houseprices/n/:neighbourhood', function($neighbourhood) {
            outputJson(HousePrices::getNeighbourhoodData($neighbourhood));
        });

$app->get('/houseprices', function() {
            outputJson(HousePrices::getAllNeighbourhoodsNormalized());
        });

$app->get('/housepricesi', function() {
            $cacheFile = './cache/cachedHousePrices.json';
            if (!file_exists($cacheFile)) {
                $housePriceData = HousePrices::getHousePriceDataByNeighbourhoodInterpolated();
                touch($cacheFile);
                file_put_contents($cacheFile, jsonp_encode($housePriceData));
                outputJson($housePriceData);
            } else {

                header("Content-Type: application/json");
                echo file_get_contents($cacheFile);
                exit;
            }
        });

$app->get('/areadata', function() {
            $areaCode = (isset($_GET['area_code'])) ? $_GET['area_code'] : null;

            $lat = (isset($_GET['lat'])) ? $_GET['lat'] : null;
            $lon = (isset($_GET['lon'])) ? $_GET['lon'] : null;
            if (!empty($areaCode)) {
                $area = new Area($areaCode);
                outputJson($area->getAreaData());
            } else if (!empty($lat) && !empty($lon)) {
                $geoData = new Geo($lat, $lon);
                outputJson($geoData->getAreaData());
            } else {
                outputJson(null);
            }
        });

$app->get('/ks2_data', function() {
            outputJson(Schools::getAllKS2DataByNeighbourhood());
        });

$app->get('/ks4_data', function() {
            outputJson(Schools::getAllKS4DataByNeighbourhood());
        });

$app->get('/mappolygons', function() {

            $connection = DAO::getConnection();
            $neighbourhoods = $connection->query("SELECT neighbourhood_code, AsText(polygon) as polygon from neighbourhoods WHERE area_id != 0");

            $outputJSON = array();
            while ($neighbourhood = $neighbourhoods->fetch_object()) {
                $polygonWKT = $neighbourhood->polygon;
                preg_match('/POLYGON\(\((.*)\)\)/', $polygonWKT, $polygon);


                $polygonPoints = explode(',', $polygon[1]);
                $outputPoints = array();
                foreach ($polygonPoints as $points) {
                    $point = explode(' ', $points);
                    $outputPoints[] = array((double) $point[1], (double) $point[0]);
                }

                $outputJSON[$neighbourhood->neighbourhood_code] = $outputPoints;
            }
            outputJson($outputJSON);
        });


/* For combined
  0 = crime
  1 = primary schooling
  2 = secondary
  3 = house price
  4 = employment
 */

function processCombi($order, $budget = null) {
    $callback = (isset($_GET['callback'])) ? $_GET['callback'] : '';
    $cacheFile = './cache/' . md5($order . $budget . $callback) . '.json';
    if (!file_exists($cacheFile)) {
        $weightings = $GLOBALS['WEIGHTINGS'];
        $numCriteria = strlen($order);

        $outputArray = array();
        if ($numCriteria <= 5) {
            for ($i = 0; $i < $numCriteria; $i++) {
                $criteria = (int) $order[$i];
                switch ($criteria) {
                    case 0:
                        $mergeArray = Crime::getAllNeighbourhoodsNormalized();
                        break;
                    case 1:
                        $mergeArray = Schools::getAllKS2DataByNeighbourhood();
                        break;
                    case 2:
                        $mergeArray = Schools::getAllKS4DataByNeighbourhood();
                        break;
                    case 3:
                        $mergeArray = ($budget != null) ? HousePrices::getHousePriceDataByNeighbourhoodInterpolated($budget) : HousePrices::getHousePriceDataByNeighbourhoodInterpolated();
                        break;
                    case 4:
                        $mergeArray = Employment::getAllNeighbourhoodsNormalized();
                        break;
                    default:
                        $mergeArray = Crime::getAllNeighbourhoodsNormalized();
                }

                foreach ($mergeArray as $key => $value) {
                    @$outputArray[$key] += $weightings[$i] * $value;
                }
            }
        }

        $maximumValue = max($outputArray);
        //Normalize
        foreach ($outputArray as $key => $value) {
            $outputArray[$key] = $value / $maximumValue;
        }

        touch($cacheFile);
        file_put_contents($cacheFile, jsonp_encode($outputArray));
        outputJson($outputArray);
    } else {
        header("Content-Type: application/json");
        echo file_get_contents($cacheFile);
        exit;
    }
}

$app->get('/combi/:order', function($order) {
            processCombi($order);
        });

$app->get('/combi/:order/:budget', function($order, $budget) {
            processCombi($order, $budget);
        });

$app->get('/listings', function () {
            require 'lib/Nestoria.php';

            $lat = (isset($_GET['lat'])) ? $_GET['lat'] : null;
            $lon = (isset($_GET['lon'])) ? $_GET['lon'] : null;
            $budget = (isset($_GET['budget'])) ? $_GET['budget'] : null;
            $bathrooms = (isset($_GET['bathrooms'])) ? $_GET['bathrooms'] : null;
            $bedrooms = (isset($_GET['bedrooms'])) ? $_GET['bedrooms'] : null;
            $type = (isset($_GET['type'])) ? $_GET['type'] : null;
            $budgetMin = $budget - ($budget * 0.25);
            $budgetMax = $budget + ($budget * 0.25);

            $values = array();
            $values['requestCountry'] = 'UK';
            $values['requestEncoding'] = 'json';
            $values['searchCentrePoint'] = "{$lat},{$lon}";
            $values['filterPropertyType'] = $type;
            $values['searchResults'] = 15;
            $values['filterListingType'] = 'buy';
            $values['filterPriceMax'] = $budgetMax;
            $values['filterPriceMin'] = $budgetMin;
            $values['filterBedroomMax'] = $bedrooms;
            $values['filterBedroomMin'] = $bedrooms;
            $values['filterBathroomMax'] = $bathrooms;
            $values['filterBathroomMin'] = $bathrooms;
            $values['requestSort'] = 'distance';

            $nestoria = new Nestoria_Nestoria($values);

            $allListings = array();
            foreach ($nestoria->decodedData->response->listings as $listing) {

                if (!empty($listing->latitude) && !(empty($listing->longitude))) {
                    $jsonListing = new stdClass();
                    $jsonListing->lat = $listing->latitude;
                    $jsonListing->lon = $listing->longitude;
                    $jsonListing->title = $listing->title;
                    $jsonListing->bathrooms = $listing->bathroom_number;
                    $jsonListing->bedrooms = $listing->bedroom_number;
                    $jsonListing->price = $listing->price_high;
                    $jsonListing->url = $listing->lister_url;
                    $jsonListing->type = (!empty($listing->property_type)) ? $listing->property_type : "";
                    $allListings[] = $jsonListing;
                }
            }

            outputJson($allListings);
            //get area data
            //compare to profile
            //query nestoria
        });

$app->get('/nearbylistings', function() {
            $weightings = $GLOBALS['WEIGHTINGS'];

            $lat = (isset($_GET['lat'])) ? $_GET['lat'] : null;
            $lon = (isset($_GET['lon'])) ? $_GET['lon'] : null;
            $order = (isset($_GET['order'])) ? $_GET['order'] : null;
            $geoData = new Geo($lat, $lon);
            $neighbourhoodCode = $geoData->getNeighbourhoodCode();

            $crimeData = Crime::getNeighbourhoodDataNormalized($neighbourhoodCode);
            $employmentData = Employment::getNeighbourhoodNormalized($neighbourhoodCode);
            $ks2Data = Schools::getKS2DataByNeighbourhood($neighbourhoodCode);
            $ks4Data = Schools::getKS4DataByNeighbourhood($neighbourhoodCode);

            $numCriteria = strlen($order);

            $totalAverage = 0;
            if ($numCriteria <= 5) {
                for ($i = 0; $i < $numCriteria; $i++) {
                    $criteria = (int) $order[$i];
                    switch ($criteria) {
                        case 0:
                            $totalAverage += $weightings[$i] * $crimeData;
                            break;
                        case 1:
                            $totalAverage += $weightings[$i] * $ks2Data;
                            break;
                        case 2:
                            $totalAverage += $weightings[$i] * $ks4Data;
                            break;
                        case 3:
                            break;
                        case 4:
                            $totalAverage += $weightings[$i] * $employmentData;
                            break;
                        default:
                            $totalAverage += $weightings[$i] * $crimeData;
                            break;
                    }
                }
            }

            $threshold = 0.5;

            if ($totalAverage < $threshold) {
                require 'lib/Nestoria.php';

                $lat = (isset($_GET['lat'])) ? $_GET['lat'] : null;
                $lon = (isset($_GET['lon'])) ? $_GET['lon'] : null;
                $budget = (isset($_GET['budget'])) ? $_GET['budget'] : null;
                $bathrooms = (isset($_GET['bathrooms'])) ? $_GET['bathrooms'] : null;
                $bedrooms = (isset($_GET['bedrooms'])) ? $_GET['bedrooms'] : null;
                $type = (isset($_GET['type'])) ? $_GET['type'] : null;
                $budgetMin = $budget - ($budget * 0.25);
                $budgetMax = $budget + ($budget * 0.25);

                $values = array();
                $values['requestCountry'] = 'UK';
                $values['requestEncoding'] = 'json';
                $values['searchCentrePoint'] = "{$lat},{$lon},0.4km";
                $values['filterListingType'] = $type;
                $values['searchResults'] = 1;
                $values['filterListingType'] = 'buy';
                $values['filterPriceMax'] = $budgetMax;
                $values['filterPriceMin'] = $budgetMin;
                $values['filterBedroomMax'] = $bedrooms;
                $values['filterBedroomMin'] = $bedrooms;
                $values['filterBathroomMax'] = $bathrooms;
                $values['filterBathroomMin'] = $bathrooms;
                $values['requestSort'] = 'distance';

                $nestoria = new Nestoria_Nestoria($values);

                $allListings = array();
                foreach ($nestoria->decodedData->response->listings as $listing) {
                    $jsonListing = new stdClass();
                    $jsonListing->lat = $listing->latitude;
                    $jsonListing->lon = $listing->longitude;
                    $jsonListing->title = $listing->title;
                    $jsonListing->bathrooms = $listing->bathroom_number;
                    $jsonListing->bedrooms = $listing->bedroom_number;
                    $jsonListing->price = $listing->price_high;
                    $jsonListing->url = $listing->lister_url;

                    $allListings[] = $jsonListing;
                }

                outputJson($allListings);
            } else {
                outputJson(array());
            }
        });

//todo iPhone notifications

$app->run();
?>