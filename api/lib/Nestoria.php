<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Nestoria API script.
 *
 * This module implements functions for harvesting data from www.nestoria.co.uk
 * through their public API.
 *
 * PHP versions 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Nestoria
 * @package   Nestoria
 * @author    Triangle Solutions Ltd <iwarner@triangle-solutions.com>
 * @copyright 2000-2007 Triangle Solutions Ltd
 * @license   http://www.php.net/license/3_0.txt PHP License 3.0
 * @version   SVN: $Id: $
 * @link      http://www.nestoria.co.uk/
 */
/**
 * Require the JSON package to decode the JSON returned by Nestoria API
 */
// require_once 'JSON.php';
// {{{ constants
// }}}
// {{{ GLOBALS
// }}}
// {{{ Nestoria_Nestoria

/**
 * @category Nestoria
 * @package  Nestoria
 */
class Nestoria_Nestoria {
    // {{{ properties

    /**
     * Sets the filter for bedroom max command max / number
     *
     * @var string $filters_bedroomMax
     */
    private $filterBedroomMax = 'max';

    /**
     * Sets the filter for bedroom min command min / number
     *
     * @var string $filters_bedroomMin
     */
    private $filterBedroomMin = 'min';

    /**
     * Sets the filter for listing type command buy / let or share
     *
     * @var string $search_SW
     */
    private $filterListingType = 'rent';

    /**
     * Sets the filter for price max command max / number
     *
     * @var string $filters_priceMax
     */
    private $filterPriceMax = 'max';

    /**
     * Sets the filter for price min command min / number
     *
     * @var string $filters_priceMin
     */
    private $filterPriceMin = 'min';

    /**
     * Sets the filter for property type command all / house / flat
     *
     * @var string $filters_propertyType Can be all / house / flat
     */
    private $filterPropertyType = 'all';

    /**
     * UK Keywords, valid keywords and keywords_exclude
     *
     * @var array $keywords_uk
     */
    private $keywordsUK = array(
        'apartment', 'attic', 'auction', 'balcony', 'basement', 'bungalow', 'cellar',
        'conservatory', 'conversion', 'cottage', 'detached', 'detached_garage',
        'dishwasher', 'double_garage', 'excouncil', 'fireplace', 'flat', 'flatshare',
        'freehold', 'furnished', 'garage', 'garden', 'grade_ii', 'gym', 'high_ceilings',
        'hot_tub', 'leasehold', 'lift', 'loft', 'lower_ground_floor', 'maisonette',
        'mews', 'new_build', 'off_street_parking', 'parking', 'penthouse', 'porter',
        'purpose_built', 'refurbished', 'sauna', 'sealed_bid', 'semi_detached',
        'share_freehold', 'shared_garden', 'swimming_pool', 'terrace', 'unfurnished',
        'victorian', 'wood_floor');

    /**
     * ES Spanish Keywords, valid keywords and keywords_exclude
     *
     * @var array $keywords_es
     */
    private $keywordsES = array(
        'adosado', 'amueblado', 'apartamento', 'atico', 'balcon', 'buhardilla',
        'bungalow', 'chimenea', 'doble_garaje', 'ex_vpo', 'garaje', 'garaje_particular',
        'gimnasio', 'invernadero', 'jacuzzi', 'jardin', 'jardin_comunitario', 'loft',
        'obra_nueva', 'piscina', 'piscina_comunitaria', 'piscina_individual',
        'piso_compartido', 'pista_de_deportes', 'planta_baja', 'playa_cercana',
        'plaza_de_aparcamiento', 'portero', 'sauna', 'semi_amueblado', 'sin_ambueblar',
        'sin_ascensor', 'sotano', 'suelo_de_madera', 'terraza', 'villa');

    /**
     * Sets the action command search_listings / echo - using echo will basically
     * test the connection to Nestoria.
     *
     * @var string $request_action Can be search_listings or echo
     */
    private $requestAction = 'search_listings';

    /**
     * Sets a definable geographic country, this can be UK (uk) or Spain (es)
     *
     * @var string $request_country
     */
    private $requestCountry = 'UK';

    /**
     * Sets the encoding command
     *
     * @var string $request_encoding Can bs json or xml
     */
    private $requestEncoding = 'json';

    /**
     * Whether to allow the JSON to be returned as pretty or not
     *
     * @var boolean $request_pretty 1 or 0
     */
    private $requestPretty = 1;

    /**
     * Var to create the sorting mechanisms choices are:
     *
     * bedroom_lowhigh
     * bedroom_highlow
     * price_lowhigh
     * price_highlow
     *
     * @var boolean $request_sort
     */
    private $requestSort = 'nestoria_rank';

    /**
     * Sets the Nestoria URL for the UK listings, this is not called directly
     * but built from the request_country
     *
     * @var string $request_uri_uk
     */
    private $requestUriUK = 'http://api.nestoria.co.uk/api';

    /**
     * Sets the Nestoria URL for the Spanish listings, this is not called directly
     * but built from the request_country
     *
     * @var string $request_uri_es
     */
    private $requestUriES = 'http://api.nestoria.es/api';

    /**
     * Sets the Nestoria URL for the Italian listings, this is not called directly
     * but built from the request_country
     *
     * @var string $request_uri_it
     */
    private $requestUriIT = 'http://api.nestoria.it/api';

    /**
     * Sets the Nestoria URL for the German listings, this is not called directly
     * but built from the request_country
     *
     * @var string $request_uri_it
     */
    private $requestUriDE = 'http://api.nestoria.de/api';

    /**
     * Sets the version command
     *
     * @var string $request_version
     * private $requestVersion = '1.14';
     */

    /**
     * Sets the centre point command lat / long
     *
     * @var string $search_centrePoint
     */
    private $searchCentrePoint = null;

    /**
     * Sets the place_name command
     *
     * @var string $search_placeName
     */
    private $searchPlaceName = 'east+grinstead';

    /**
     * Sets the North East boundary corner command lat / long
     *
     * @var string $search_NE
     */
    private $searchNE = null;

    /**
     * Sets the number of results command
     *
     * @var string $search_results Max of 50
     */
    private $searchResults = 5;

    /**
     * Sets the South West boundary corner command lat / long
     *
     * @var string $search_SW
     */
    private $searchSW = null;

    // }}}
    // {{{ __construct()

    /**
     * Constructor method, accepts the incoming paramaters array and overides
     * any class vars in needs to.
     *
     * @param  array $params Array of options that overide the class vars.
     * @return void
     */
    public function __construct($params = false) {
        $classVars = get_class_vars(get_class($this));

        // Make the default Class Vars as a simple request.
        $this->requestParams = $classVars;

        // Check that there are Parameters passed in and it is an array
        if (is_array($params)) {

            // Overide Class vars that are set with user defined params.
            foreach ($params as $k => $v) {
                $this->requestParams[$k] = urlencode(trim($v));
            }
        }

        // Sets up the class vars to be returned for debugging if necessary.
        $this->classVars = $classVars;

        // Send the request
        self::Services_NestoriaConnect();
    }

    // }}}
    // {{{ Services_NestoriaConnect()

    /**
     * Method to connect to Nestoria property API and retrieve JSON text based
     * on the user defined search paramaters and filters.
     *
     * @return object $decoded JSON decoded object
     */
    private function Services_NestoriaConnect() {
        // Create the required filters and request options for the Nestoria API
        $options = array(
            // Search results
            'number_of_results' => $this->requestParams['searchResults'],
            // Filters
            'bedroom_max' => $this->requestParams['filterBedroomMax'],
            'bedroom_min' => $this->requestParams['filterBedroomMin'],
            'bathroom_max' => $this->requestParams['filterBathroomMax'],
            'bathroom_min' => $this->requestParams['filterBathroomMin'],
            'listing_type' => $this->requestParams['filterListingType'],
            'price_max' => $this->requestParams['filterPriceMax'],
            'price_min' => $this->requestParams['filterPriceMin'],
            'property_type' => $this->requestParams['filterPropertyType'],
            // Request elements
            'action' => $this->requestParams['requestAction'],
            'country' => $this->requestParams['requestCountry'],
            'language' => 'en',
            'encoding' => $this->requestParams['requestEncoding'],
            'pretty' => $this->requestParams['requestPretty'],
            'sort' => $this->requestParams['requestSort'],
                // 'keywords' => $this->requestParams['keywords'],
                // 'keywords_exclude' => $this->requestParams['keywordsExclude']
        );

        // Create the search paramaters, as not all of these can be used we need
        // to ascertain which the user wants in the request. So follow the order
        // below and silently ignore options that are still null.
  
        if (isset($this->requestParams['searchCentrePoint'])) {
            $options += array('centre_point' => $this->requestParams['searchCentrePoint']);
        }
        // Loop through these options and create the GET request string.
        $requestString = '?';

        foreach ($options as $k => $v) {
            $requestString .= $k . '=' . $v . '&';
        }

        // Get the correct Request URL based on the Country selected
        $var = 'requestUri' . strtoupper($this->requestParams['requestCountry']);

        // Send the HTTP_Request.
        if (ini_get(' allow_url_fopen')) {
            $body = file_get_contents($this->$var . $requestString);
        } else {
            $ch = curl_init();
            $timeout = 5; // set to zero for no timeout
            curl_setopt($ch, CURLOPT_URL, $this->$var . $requestString);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $body = curl_exec($ch);
            curl_close($ch);
        }
        if (!empty($body)) {

            // Set up the Nestoria JSON for debugging purposes.
            $this->nestoriaData = htmlentities($body);

            // Finally return the JSON that was decoded.
            $this->decodedData = self::Services_NestoriaDecode($body);
        } else {
            return false;
        }
    }

    // }}}
    // {{{ Services_NestoriaDecode()

    /**
     * Method to decode the returned Nestoria data with the desired decoder.
     *
     * @param  string $body Nestoria data returned as XML or JSON
     */
    private function Services_NestoriaDecode($body) {
        // Decode the returned JSON
        if ($this->requestParams['requestEncoding'] == 'json') {

            // Instantiate the JSON class and decode the Nestoria JSON
            $decoded = json_decode($body);

            // Decode the returned XML
        } else {

            // Instantiate the JSON class and decode the Nestoria JSON
            $xml = simplexml_load_string($body);
            $decoded = (array) $xml;
        }

        return $decoded;
    }

    // }}}
}

// }}}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */