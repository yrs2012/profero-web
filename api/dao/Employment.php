<?php

class Employment extends DAO {

    public static function getAllNeighbourhoodsNormalized() {
        $query = "select neighbourhood_code, data/(select max(data) from employment_data WHERE year=2011) as normalized_data
        from neighbourhoods
        right join employment_data on employment_data.area_id = neighbourhoods.area_id
        where year =2011";

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->execute();

        $stmt->bind_result($neighbourhoodCode, $normalizedData);

        $allNeighbourhoods = array();
        while ($stmt->fetch()) {
            $allNeighbourhoods[$neighbourhoodCode] = (double) $normalizedData;
        }

        return $allNeighbourhoods;
    }

    public static function getNeighbourhoodNormalized($neighbourhood) {
        $query = "select data/(select max(data) from employment_data WHERE year=2011) as normalized_data
        from neighbourhoods
        left join employment_data on employment_data.area_id = neighbourhoods.area_id
        where neighbourhood_code = ? AND year =2011
        limit 1";

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->bind_param('s', $neighbourhood);

        $stmt->execute();
        $stmt->bind_result($normalizedData);
        $stmt->fetch();

        return $normalizedData;
    }

}

?>
