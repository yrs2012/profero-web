<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Area
 *
 * @author Ains
 */
class Area {

    private $areaCode;

    public function Area($areaCode) {
        $this->areaCode = $areaCode;
    }

    public function getAreaData() {
        $connection = DAO::getConnection();
        $query = 'select area_name, neighbourhood_code, price_data.data as avg_house_price, employment_data.data as unemployment_percentage, crime_data.data as crime_totals FROM `neighbourhoods`
right join employment_data on employment_data.area_id = neighbourhoods.area_id
right join price_data on price_data.area_id = neighbourhoods.area_id
left join crime_data on crime_data.neighbourhood_id = neighbourhoods.id
left join areas on areas.id = neighbourhoods.area_id
where neighbourhood_code = ? 
LIMIT 1';

        $stmt = $connection->prepare($query);
        $stmt->bind_param('s', $this->areaCode);
        $stmt->execute();
        $stmt->bind_result($areaName, $areaCode, $avgHousePrice, $unemploymentPercent, $crimeTotals);
        $stmt->fetch();

        $areaData = new StdClass;
        $areaData->areaCode = $areaCode;
        $areaData->borough_name = $areaName;
        $areaData->avgHousePrice = $avgHousePrice;
        $areaData->unploymentPercent = $unemploymentPercent;
        $areaData->crimeTotals = $crimeTotals;

        return $areaData;
    }

}

?>
