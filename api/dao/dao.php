<?php

class DAO {

    public $connection;

    public function DAO() {
        //  $this->connection = new mysqli('localhost:8889', 'root', 'root', 'yrsapp');
    }

    public static function getConnection() {
        $servicesJson = json_decode(getenv("VCAP_SERVICES"), true);
        if (!empty($servicesJson)) {
            $mysqlConfig = $servicesJson["mysql-5.1"][0]["credentials"];
            $username = $mysqlConfig["username"];
            $password = $mysqlConfig["password"];
            $hostname = $mysqlConfig["hostname"];
            $port = $mysqlConfig["port"];
            $db = $mysqlConfig["name"];
        }
        else {
            $username = 'root';
            $password = 'root';
            $hostname = '127.0.0.1';
            $port = '8889';
            $db ='yrsapp';
        }
        return new mysqli($hostname, $username, $password, $db, $port);
    }
}

?>
