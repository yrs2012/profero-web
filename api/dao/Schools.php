<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Schools
 *
 * @author Ains
 */
class Schools {

    public static function getSchoolDataByNeighbourhood($table) {
        $query = "select neighbourhood_code, avg(data) as normalized_data
        from neighbourhoods
        left join {$table} on {$table}.neighbourhood_id = neighbourhoods.id
        WHERE area_id != 0
        group by neighbourhood_code";

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->execute();

        $stmt->bind_result($neighbourhoodCode, $normalizedData);

        $allNeighbourhoods = array();
        $failedLookups = array();
        while ($stmt->fetch()) {
            if ($normalizedData != NULL) {
                $dataRow = array('neighbourhoodCode' => $neighbourhoodCode, 'data' => $normalizedData);
                $allNeighbourhoods[] = $dataRow;
            } else {
                $failedLookups[] = array(count($allNeighbourhoods), $neighbourhoodCode);
            }
        }

        foreach ($failedLookups as $lookup) {
            $neighbourhoodCount = count($allNeighbourhoods);
            $lookupCode = $lookup[1];
            $lookForward = false;
            $lookBack = false;

            for ($i = $lookup[0]; $i < $neighbourhoodCount; $i++) {
                $neighbourhoodCode = $allNeighbourhoods[$i];
                if (substr($neighbourhoodCode['neighbourhoodCode'], 0, 4) == substr($lookupCode, 0, 4)) {
                    $lookForward = abs($lookup[0] - $i);
                    $lookForwardIndex = $i;
                }
            }

            for ($i = $lookup[0]; $i > 0; $i--) {
                $neighbourhoodCode = $allNeighbourhoods[$i];
                if (substr($neighbourhoodCode['neighbourhoodCode'], 0, 4) == substr($lookupCode, 0, 4)) {
                    $lookBack = abs($lookup[0] - $i);
                    $lookBackIndex = $i;
                }
            }

            if ($lookForward || $lookBack) {
                if ((!$lookBack && $lookForward) || ($lookForward < $lookBack && $lookForward)) {
                    $dataRow = array('neighbourhoodCode' => $lookupCode, 'data' => $allNeighbourhoods[$lookForwardIndex]['data']);
                    $allNeighbourhoods[] = $dataRow;
                } else if ($lookBack) {
                    $dataRow = array('neighbourhoodCode' => $lookupCode, 'data' => $allNeighbourhoods[$lookBackIndex]['data']);
                    $allNeighbourhoods[] = $dataRow;
                }
            }
        }

        $maximumScore = 0;
        foreach ($allNeighbourhoods as $neighbourhood) {
            if ($neighbourhood['data'] > $maximumScore) {
                $maximumScore = $neighbourhood['data'];
            }
        }

        $outputNeighbourhoods = array();
        foreach ($allNeighbourhoods as $neighbourhood) {
            $outputNeighbourhoods[$neighbourhood['neighbourhoodCode']] = (1 - ($neighbourhood['data'] / $maximumScore));
        }


        return $outputNeighbourhoods;
    }

    public static function getAllKS2DataByNeighbourhood() {
        return Schools::getSchoolDataByNeighbourhood('ks2_data');
    }

    public static function getAllKS4DataByNeighbourhood() {
        return Schools::getSchoolDataByNeighbourhood('ks4_data');
    }

    public static function getKS2DataByNeighbourhood($neighbourhood) {
        $allSchools = Schools::getSchoolDataByNeighbourhood('ks2_data');
        return $allSchools[$neighbourhood];
    }

    public static function getKS4DataByNeighbourhood($neighbourhood) {
        $allSchools = Schools::getSchoolDataByNeighbourhood('ks4_data');
        return $allSchools[$neighbourhood];
    }

}

?>
