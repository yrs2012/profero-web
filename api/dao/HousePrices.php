<?php

class HousePrices extends DAO {

    public static function getAllNeighbourhoodsNormalizedByBudget($budget) {
        $budget = (int) $budget;
        $query = "select neighbourhood_code, abs(({$budget} - data))/(select max(abs({$budget} - data)) from price_data) as normalized_data
        from neighbourhoods
        right join price_data on price_data.area_id = neighbourhoods.area_id";

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->execute();

        $stmt->bind_result($neighbourhoodCode, $normalizedData);

        $allNeighbourhoods = array();
        while ($stmt->fetch()) {
            $allNeighbourhoods[$neighbourhoodCode] = (double) $normalizedData;
        }

        return $allNeighbourhoods;
    }

    public static function getAllNeighbourhoodsNormalized() {
        $query = "select neighbourhood_code, data/(select max(data) from price_data) as normalized_data
        from neighbourhoods
        right join price_data on price_data.area_id = neighbourhoods.area_id";

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->execute();

        $stmt->bind_result($neighbourhoodCode, $normalizedData);

        $allNeighbourhoods = array();
        while ($stmt->fetch()) {
            $allNeighbourhoods[$neighbourhoodCode] = (double) $normalizedData;
        }

        return $allNeighbourhoods;
    }

    public static function getNeighbourhoodData($neighbourhood) {
        $query = "select data as normalized_data
        from neighbourhoods
        left join price_data on price_data.area_id = neighbourhoods.area_id
        where neighbourhood_code = ?
        limit 1";

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->bind_param('s', $neighbourhood);

        $stmt->execute();
        $stmt->bind_result($normalizedData);
        $stmt->fetch();

        return $normalizedData;
    }

    public static function getNeighbourhoodDataNormalized($neighbourhood) {
        $query = "select data/(select max(data) from price_data) as normalized_data
        from neighbourhoods
        left join price_data on price_data.area_id = neighbourhoods.area_id
        where neighbourhood_code = ?
        limit 1";

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->bind_param('s', $neighbourhood);

        $stmt->execute();
        $stmt->bind_result($normalizedData);
        $stmt->fetch();

        return $normalizedData;
    }

    public static function getNeighbourhoodDataNormalizedByBudget($neighbourhood, $budget) {
        $budget = (int) $budget;
        $query = "select abs(({$budget} - data))/(select max(abs({$budget} - data)) from price_data) as normalized_data
        from neighbourhoods
        right join price_data on price_data.area_id = neighbourhoods.area_id
        where neighbourhood_code = ?
        limit 1";

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->bind_param('s', $neighbourhood);
        $stmt->execute();

        $stmt->bind_result($normalizedData);
        $stmt->fetch();

        return $normalizedData;
    }

    public static function getHousePriceDataByNeighbourhoodInterpolated($budget = false) {

        if ($budget) {
            $query = "select neighbourhood_code,  abs(({$budget} - data))/(select max(abs({$budget} - data)) from price_data_nxp) as normalized_data
        from neighbourhoods
        left join price_data_nxp on price_data_nxp.neighbourhood_id = neighbourhoods.id
        WHERE area_id != 0
        group by neighbourhood_code";
        } else {
            $query = "select neighbourhood_code, avg(data)/(select max(data) from price_data_nxp) as normalized_data
        from neighbourhoods
        left join price_data_nxp on price_data_nxp.neighbourhood_id = neighbourhoods.id
        WHERE area_id != 0
        group by neighbourhood_code";
        }

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->execute();

        $stmt->bind_result($neighbourhoodCode, $normalizedData);

        $allNeighbourhoods = array();
        $failedLookups = array();
        while ($stmt->fetch()) {
            if ($normalizedData != NULL) {
                $dataRow = array('neighbourhoodCode' => $neighbourhoodCode, 'data' => $normalizedData);
                $allNeighbourhoods[] = $dataRow;
            } else {
                $failedLookups[] = array(count($allNeighbourhoods), $neighbourhoodCode);
            }
        }

        foreach ($failedLookups as $lookup) {
            $neighbourhoodCount = count($allNeighbourhoods);
            $lookupCode = $lookup[1];
            $lookForward = false;
            $lookBack = false;

            for ($i = $lookup[0]; $i < $neighbourhoodCount; $i++) {
                $neighbourhoodCode = $allNeighbourhoods[$i];
                if (substr($neighbourhoodCode['neighbourhoodCode'], 0, 4) == substr($lookupCode, 0, 4)) {
                    $lookForward = abs($lookup[0] - $i);
                    $lookForwardIndex = $i;
                }
            }

            for ($i = $lookup[0]; $i > 0; $i--) {
                $neighbourhoodCode = $allNeighbourhoods[$i];
                if (substr($neighbourhoodCode['neighbourhoodCode'], 0, 4) == substr($lookupCode, 0, 4)) {
                    $lookBack = abs($lookup[0] - $i);
                    $lookBackIndex = $i;
                }
            }

            if ($lookForward || $lookBack) {
                if ((!$lookBack && $lookForward) || ($lookForward < $lookBack && $lookForward)) {
                    $dataRow = array('neighbourhoodCode' => $lookupCode, 'data' => $allNeighbourhoods[$lookForwardIndex]['data']);
                    $allNeighbourhoods[] = $dataRow;
                } else if ($lookBack) {
                    $dataRow = array('neighbourhoodCode' => $lookupCode, 'data' => $allNeighbourhoods[$lookBackIndex]['data']);
                    $allNeighbourhoods[] = $dataRow;
                }
            }
        }

        $maximumScore = 0;
        foreach ($allNeighbourhoods as $neighbourhood) {
            if ($neighbourhood['data'] > $maximumScore) {
                $maximumScore = $neighbourhood['data'];
            }
        }

        $outputNeighbourhoods = array();
        foreach ($allNeighbourhoods as $neighbourhood) {
            $outputNeighbourhoods[$neighbourhood['neighbourhoodCode']] = ($neighbourhood['data'] / $maximumScore);
        }


        return $outputNeighbourhoods;
    }

}

?>
