<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Geo
 *
 * @author Ains
 */
class Geo {

    private $lat;
    private $lon;

    public function Geo($lat, $lon) {
        $this->lat = $lat;
        $this->lon = $lon;
    }

    private function toMysqlPoint() {
        return "POINT({$this->lon} {$this->lat})";
    }

    public function getNeighbourhoodCode() {
        $connection = DAO::getConnection();
        $query = 'select neighbourhood_code FROM `neighbourhoods`
        where Contains(polygon, GeomFromText(?)) AND polygon is not null
        LIMIT 1';

        $stmt = $connection->prepare($query);
        $mysqlPoint = $this->toMysqlPoint();
        $stmt->bind_param('s', $mysqlPoint);
        $stmt->execute();
        $stmt->bind_result($neighbourhoodCode);
        $stmt->fetch();

        return $neighbourhoodCode;
    }

    public function getAreaData() {
        $connection = DAO::getConnection();
        $query = 'select area_name, neighbourhood_code, price_data_nxp.data as avg_house_price, employment_data.data as unemployment_percentage, police_data_n.data as crime_totals, bur, asb, robbery, vc, vi, pdaw, sl, cd, ot, dr, other FROM `neighbourhoods`
right join employment_data on employment_data.area_id = neighbourhoods.area_id
left join price_data_nxp on price_data_nxp.neighbourhood_id = neighbourhoods.id
left join police_data_n on police_data_n.neighbourhood_id = neighbourhoods.id
left join areas on areas.id = neighbourhoods.area_id
where Contains(polygon, GeomFromText(?)) AND polygon is not null 
LIMIT 1';

        
        $stmt = $connection->prepare($query);
        $mysqlPoint = $this->toMysqlPoint();
        $stmt->bind_param('s', $mysqlPoint);
        $stmt->execute();
        $stmt->bind_result($areaName, $areaCode, $avgHousePrice, $unemploymentPercent, $crimeTotals, $bur, $asb, $robbery, $vc, $vi, $pdaw, $sl, $cd, $ot, $dr, $other);
        $stmt->fetch();

        $areaData = new StdClass;
        $areaData->areaCode = $areaCode;
        $areaData->borough_name = $areaName;
        $areaData->avgHousePrice = $avgHousePrice;
        $areaData->unploymentPercent = $unemploymentPercent;
        $areaData->crimeTotals = $crimeTotals;
        $areaData->crime['burglary'] = $bur;
        $areaData->crime['anti_social_behaviour'] = $asb;
        $areaData->crime['robbery'] = $robbery;
        $areaData->crime['vehicle_crime'] = $vc;
        $areaData->crime['violent_crime'] = $vi;
        $areaData->crime['public_disorder'] = $pdaw;
        $areaData->crime['shoplifting'] = $sl;
        $areaData->crime['criminal_damage'] = $cd;
        $areaData->crime['other_theft'] = $ot;
        $areaData->crime['drugs'] = $dr;
        $areaData->crime['other'] = $other;
        return $areaData;
    }

}

?>
