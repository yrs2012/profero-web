<?php

class Crime extends DAO {

    public static function getNeighbourhoodData($neighbourhood) {
        $query = "select data
        from neighbourhoods
        left join crime_data on crime_data.neighbourhood_id = neighbourhoods.id
        where neighbourhood_code = ?
        limit 1";

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->bind_param('s', $neighbourhood);

        $stmt->execute();
        $stmt->bind_result($data);
        $stmt->fetch();

        return $data;
    }

    public static function getNeighbourhoodDataNormalized($neighbourhood) {
        $query = "select data/(select max(data) from crime_data)
        from neighbourhoods
        left join crime_data on crime_data.neighbourhood_id = neighbourhoods.id
        where neighbourhood_code = ?
        limit 1";

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->bind_param('s', $neighbourhood);

        $stmt->execute();
        $stmt->bind_result($data);
        $stmt->fetch();

        return $data;
    }

    public static function getAllNeighbourhoodsNormalized() {
        $query = "select neighbourhood_code, data/(select max(data) from crime_data) as normalized_data
        from neighbourhoods
        right join crime_data on crime_data.neighbourhood_id = neighbourhoods.id";

        $connection = DAO::getConnection();
        $stmt = $connection->prepare($query);
        $stmt->execute();

        $stmt->bind_result($neighbourhoodCode, $normalizedData);

        $allNeighbourhoods = array();
        while ($stmt->fetch()) {
            $allNeighbourhoods[$neighbourhoodCode] = (double) $normalizedData;
        }

        return $allNeighbourhoods;
    }

}

?>
