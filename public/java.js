var map;
var infoWindow;
var mapOverlays = [];

Number.prototype.formatMoney = function(c, d, t){
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
 
function showArrays(neighbourhoodCode, event) {
	
    $.getJSON("http://yrs2012.eu01.aws.af.cm/api/areadata?callback=?&area_code=" + neighbourhoodCode, function(crime_amount) {
        var contentString
        if (crime_amount.borough_name == null)
        {
            contentString = "Info N/A"
        }
        else
        {
            contentString = '<b>Borough: </b>' + crime_amount.borough_name + '<br>';
            contentString += '&#8470; of crimes:' + crime_amount.crimeTotals + '<br>' + "Percentage of unemployment: " + crime_amount.unploymentPercent +"%" +'<br>' + "Average house price: &pound;" + (crime_amount.avgHousePrice).formatMoney(0, '.', ',');
        }

        // Replace our Info Window's content and position
        infowindow.setContent(contentString);
        infowindow.setPosition(event.latLng);

        infowindow.open(map);
    })
}
function showMap() {
    //var geocoder = new GClientGeocoder();
    //geocoder.setCache=null;
	
    var london = new google.maps.LatLng(51.5171,-0.1062);
    var mapOptions = {
        zoom: 10,
        center: london,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var url_end = "?nocache=" + (new Date()).valueOf();
    var server_root = "http://police-data.cloudfoundry.com/data/";
    var kmlFile = server_root + "mergedKMLs.kml";
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    infowindow = new google.maps.InfoWindow();
    //var ctaLayer = new google.maps.KmlLayer(kmlFile);
    //ctaLayer.setMap(map);
    // Add controls
    //map.addControl(new GLargeMapControl());
    //map.addControl(new GMapTypeControl());
    //for (i=0;i<10;i++){
    //var geoxml = new GGeoXml(kmlFile);
    //map.addOverlay(geoxml);//}
    // Default zoom level
    //var zl = 12;
    //map.setCenter(new GLatLng(51.500152,-0.126236),zl);	}
    show_overlay([4]);
}

function clearOverlays() {
    if (mapOverlays) {
        for (var i = 0; i < mapOverlays.length; i++ ) {
            mapOverlays[i].setMap(null);
        }
    }
  
    mapOverlays = [];
}

function show_overlay(categories) {
    //var col_map = []
    //$.each(crimes, function(neighbourhoodCode, c_data) {
    //var red,grn
    //if (c_data < 0.5) {
    //red = Math.floor((c_data * 2)*255).toString(16);
    //grn = Math.floor((1)*255).toString(16);
    //}else {
    //red = Math.floor((1)*255).toString(16);
    //grn = Math.floor(((1-c_data) * 2)*255).toString(16);
    //}
    //var col_fin = (red + grn)
    //col_map[neighbourhoodCode] = "#" + col_fin + "00"
    //})
    var col_map = [];
    console.log('deeee');
    $.getJSON("http://yrs2012.eu01.aws.af.cm/api/combi/"+ categories.join("") +"?callback=?", function(employment) {
        $.each(employment, function(neighbourhoodCode, employ_data) {
            var red,grn
            if (employ_data < 0.5) {
                red = Math.floor((employ_data * 2)*255).toString(16);
                grn = Math.floor((1)*255).toString(16);
            }else {
                red = Math.floor((1)*255).toString(16);
                grn = Math.floor(((1-employ_data) * 2)*255).toString(16);
            }
            var col_fin = (red + grn)
            col_map[neighbourhoodCode] = "#" + col_fin + "00"
			
        })
        console.log('polygons');
        gen_polygons(col_map);
    })
}
function gen_polygons(col_map) {
    $.getJSON("http://yrs2012.eu01.aws.af.cm/api/mappolygons?callback=?", function(data) {
        
        clearOverlays();
        $.each(data, function(neighbourhoodCode,outerBoundary){
            var targetarray = new Array (outerBoundary.length)
            for (i=0;i<outerBoundary.length;i++)
            {
                //var cords=(neighbourhood.outerBoundary)
                targetarray[i] = new google.maps.LatLng(outerBoundary[i][0],outerBoundary[i][1])
            }
            var bermudaTriangle = new google.maps.Polygon({
                paths: targetarray,
                strokeColor: '#000000',
                strokeOpacity: 0.3,
                strokeWeight: 1,
                fillColor: col_map[neighbourhoodCode],
                fillOpacity: 0.3
            });
            
            mapOverlays.push(bermudaTriangle);
			
            //Infowindow
            google.maps.event.addListener(bermudaTriangle, 'click',function(event){
                showArrays(neighbourhoodCode, event)
            });
			

			
            bermudaTriangle.setMap(map);
        })
    })
	
}

$(document).ready(function() {
    $("input[type=checkbox]").change(function(){
        var selectName = $(this).attr('name');
        var selectChecked = $(this).attr('checked');
        if (selectChecked == "checked")
        {
            $("input[type=checkbox]").not("[name=" + selectName + "]").each(function()
            {
                $(this).attr('checked', false);
            });
        }
        else {
            $("input[name=crime]").attr('checked', true);
        }
        console.log(selectName);
        switch (selectName)
        {
            case "crime":
                show_overlay([0]);
                break;
            case "unemployment":
                show_overlay([4]);
                break;
            case "school":
                show_overlay([1]);
                break;
            case "school2":
                show_overlay([2]);
                break;
            default:
                console.log("Default");
                show_overlay([0]);
                break;
        }
    }); 
});
